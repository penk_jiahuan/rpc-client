<?php declare(strict_types=1);

namespace Penk\Rpc\Client\Contract;

/**
 * Class ProviderInterface
 *
 * @since 2.0
 */
interface ProviderInterface
{
    /**
     * @param Client $client
     *
     * @return array
     *
     * @example
     * [
     *     'host:port',
     *     'host:port',
     *     'host:port',
     * ]
     */
    public function getList(Client $client): array;
}
