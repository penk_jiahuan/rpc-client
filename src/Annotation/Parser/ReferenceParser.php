<?php declare(strict_types=1);

namespace Penk\Rpc\Client\Annotation\Parser;

use PhpDocReader\AnnotationException;
use PhpDocReader\PhpDocReader;
use ReflectionException;
use ReflectionProperty;
use Penk\Rpc\Client\Proxy;
use Penk\Rpc\Client\ReferenceRegister;
use Swoft\Annotation\Annotation\Mapping\AnnotationParser;
use Swoft\Annotation\Annotation\Parser\Parser;
use Swoft\Proxy\Exception\ProxyException;
use Penk\Rpc\Client\Annotation\Mapping\Reference;
use Swoft\Rpc\Client\Exception\RpcClientException;



/**
 * Class ReferenceParser
 *
 * @since 2.0
 *
 * @AnnotationParser(Reference::class)
 */
class ReferenceParser extends Parser
{
    /**
     * @param int       $type
     * @param Reference $refObject
     *
     * @return array
     * @throws RpcClientException
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws ProxyException
     */
    public function parse(int $type, $refObject): array
    {
        // Parse php document
        $phpReader       = new PhpDocReader();
        $reflectProperty = new ReflectionProperty($this->className, $this->propertyName);
        $propClassType   = $phpReader->getPropertyClass($reflectProperty);

        if (empty($propClassType)) {
            throw new RpcClientException(sprintf(
                '`@Reference`(%s->%s) must to define `@var xxx`',
                $this->className,
                $this->propertyName
            ));
        }

        $refVersion = $refObject->getVersion();
        $className  = Proxy::newClassName($propClassType, $refVersion);

        $this->definitions[$className] = [
            'class' => $className,
        ];

        ReferenceRegister::register($className, $refObject->getPool(), $refVersion);
        return [$className, true];
    }
}
